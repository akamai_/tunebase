//
//  SongTableViewCell.swift
//  TuneBase
//
//  Created by Robert Szost on 15.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class SearchSongTableViewCell: UITableViewCell {
    
    //IBOutlets
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var artworkImage: UIImageView!
    
    func updateView(song: Song){
        songLabel.text = song.title
        artistLabel.text = song.artist
        albumLabel.text = song.album
        genreLabel.text = song.genre
        
        //Get image
        let url = URL(string: song.image)
        let data = try? Data(contentsOf: url!)
        artworkImage.image = UIImage(data: data!)
    }
    
}
