//
//  SavedSongTableViewCell.swift
//  TuneBase
//
//  Created by Robert Szost on 15.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class SavedSongTableViewCell: UITableViewCell {
    
    //IBOutlets
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var artworkImage: UIImageView!
    
    func updateView(song: SavedSongs){
        songLabel.text = song.title
        artistLabel.text = song.artist
        albumLabel.text = song.album
        genreLabel.text = song.genre

//  TO DO: Problem with artworkImages when taken from CoreData
//        //Get image
//        let url = URL(string: song.imageURL)
//        let data = try? Data(contentsOf: url!)
//        artworkImage.image = UIImage(data: song.image!)
//        
    }
    
}
