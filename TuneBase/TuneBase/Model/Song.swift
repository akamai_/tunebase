//
//  Song.swift
//  TuneBase
//
//  Created by Robert Szost on 14.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import Foundation
import CoreData

class Song: NSObject {
    
    //Properties of model
    let title: String
    let album: String
    let artist: String
    let genre: String
    let image: String
    
    //Initialisation
    init(title: String, album: String, artist: String, genre: String, image: String){
        self.title = title
        self.album = album
        self.artist = artist
        self.genre = genre
        self.image = image
    }
}
