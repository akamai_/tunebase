//
//  FilterViewController.swift
//  TuneBase
//
//  Created by Robert Szost on 14.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    //IBOutlets
    @IBOutlet weak var artistTextField: UITextField!
    @IBOutlet weak var singleTextField: UITextField!
    @IBOutlet weak var albumTextField: UITextField!
    @IBOutlet weak var genreTextField: UITextField!
    @IBAction func refreshButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //TO DO: Filtering
    func filterSavedSongs(){
        
    }
}
