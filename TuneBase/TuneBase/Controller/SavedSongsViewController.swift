//
//  SavedSongsViewController.swift
//  TuneBase
//
//  Created by Robert Szost on 14.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit
import CoreData

class SavedSongsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Constants
    let contextData = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var songArray:[SavedSongs] = []
    
    //IBOutlets
    @IBOutlet var tableView: UITableView!
    
    //Function which allows to use from CoreData (works only for index 0), need to improve
    @IBAction func deleteButton(_ sender: Any) {
        self.deleteData(deleteIndex: 0)
        fetchData()
        self.tableView.reloadData()
    }
    
    //Deployes before appearing the "Saved" tab view, refreshing tableView
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        //TableView setup
        tableView.delegate = self
        tableView.dataSource = self
        
        self.fetchData()
        self.tableView.reloadData()
    }
    
    //TableView setup
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Number of rows dependend of saved result
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songArray.count
    }
    
    //Presenting cells from database
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if songArray.isEmpty == false{
            if let cell  = tableView.dequeueReusableCell(withIdentifier: "savedSongCell", for: indexPath) as? SavedSongTableViewCell{
                cell.updateView(song: songArray[indexPath[1]])
                return cell
            } else {
                return SearchSongTableViewCell()
            }
        }
        return SearchSongTableViewCell()
    }
    
    //Grabing data from database
    func fetchData(){
        do{
            songArray = try contextData.fetch(SavedSongs.fetchRequest())
        } catch {
            print(error)
        }
        self.tableView.reloadData()
    }
    
    //Function which allows to delete from CoreDate, delete entity with specified index
    func deleteData(deleteIndex: Int){
        contextData.delete(songArray[deleteIndex])
        do{
            try contextData.save()
            self.tableView.reloadData()
        }catch {
            print(error)
        }
    }
}
