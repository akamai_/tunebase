//
//  ViewController.swift
//  TuneBase
//
//  Created by Robert Szost on 11.05.2018.
//  Copyright © 2018 Robert Szost. All rights reserved.
//

import UIKit
import CoreData

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //Constants
    let BASE_URL = "https://itunes.apple.com/search?term="
    var SEARCHBAR_REQUEST = ""
    var songs = NSDictionary()
    var songList : [Song] = []
    let dataContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TableView setup
        tableView.delegate = self
        tableView.dataSource = self
        
        //SearchBar setup
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
    }
    
    //Button which allows to save found songs
    @IBAction func saveButton(_ sender: Any) {
        if songList.isEmpty == false {
            
            //Creating object
            let newSavedSong = NSEntityDescription.insertNewObject(forEntityName: "SavedSongs", into: dataContext)
            
            //Filling object
            newSavedSong.setValue(songList[0].album, forKey: "album")
            newSavedSong.setValue(songList[0].artist, forKey: "artist")
            newSavedSong.setValue(songList[0].genre, forKey: "genre")
            newSavedSong.setValue(songList[0].title, forKey: "title")
            
            do {
                try dataContext.save()
                
                //Alert for user if song added to favourites successfully
                let alertController = UIAlertController(title: "Song added", message:
                    "You saved song to your favourites.", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } catch {
                print(error)
            }
        }
    }
    
    //TableView setup
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Number of rows dependend of search result
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songList.count
    }
    
    //Presenting cells after searching
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if songList.isEmpty == false{
            if let cell  = tableView.dequeueReusableCell(withIdentifier: "songCell", for: indexPath) as? SearchSongTableViewCell{
                cell.updateView(song: songList[indexPath.row])
                return cell
            } else {
                return SearchSongTableViewCell()
            }
        }
        return SearchSongTableViewCell()
    }
    
    //Function which allows to search songs in apple database
    func findSongs(completed: @escaping () -> ()) {

        //Building URL for API (base + searchBar text)
        let SEARCH_URL = "\(BASE_URL)\(SEARCHBAR_REQUEST)&media=music"
        
        //Deleting whitespaces in URL
        let spaceToPercentageString = (SEARCH_URL as NSString).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        
        //finalURL used to get results
        let finalURL = URL(string: spaceToPercentageString!)
        
        //Sending dataTask
        URLSession.shared.dataTask(with: finalURL!) { (data, response, error) in
            
            if error == nil {
                do {
                    self.songs = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! NSDictionary
                    
                    //Parsing object to dictionary
                    let resultsArray = self.songs.object(forKey: "results") as! [Dictionary<String, Any>]
                    
                    if resultsArray.count > 0 {
                        self.songList.removeAll()
                        if let resultDict = resultsArray.first{
                            
                            let artistName = resultDict["artistName"] as! String
                            let songName = resultDict["trackName"] as! String
                            let artworkURL = resultDict["artworkUrl100"] as! String
                            let albumTitle = resultDict["collectionName"] as! String
                            let genreType = resultDict["primaryGenreName"] as! String
                            
                            //Adding found results to array
                            self.songList.append(Song(title: songName, album: albumTitle, artist: artistName, genre: genreType, image: artworkURL))
                        }
                    }
                    //Back to main thread
                    DispatchQueue.main.async {
                        completed()
                    }
                }catch {
                    print("JSON Error")
                }
            }
            }.resume()
    }
    
    //Reading input from searchBar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            view.endEditing(true)
            tableView.reloadData()
        } else {
            SEARCHBAR_REQUEST = searchBar.text!
            findSongs {
                self.tableView.reloadData()
                
            }
        }
    }
}

